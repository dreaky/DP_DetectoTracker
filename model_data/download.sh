#!/bin/sh


# Linux
wget=/usr/bin/wget


if [ ! -x "$wget" ]; then
  echo "ERROR: No wget." >&2
  exit 1
fi

if ! ${wget} "{$1}" "-qP" "$2"; then
  echo "ERROR: can't download file $URL_COCO_ANN" >&2
  exit 1
fi