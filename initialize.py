#! /usr/bin/env python
# -*- coding: utf-8 -*-

"""
More information is in convert.py this only wrap Yolo models.
for tiny versions use: YoloT3, YoloT2, YoloT2-VOC, YoloT1, YoloT1-VOC
for other use: YoloV3, YoloV2, YoloV2-VOC, YoloV1
"""

import argparse
import os

parser = argparse.ArgumentParser(description='Define model paths to Darknet To Keras Converter.')
parser.add_argument('yolo_version', help='Define which one of Yolo model will be used.')

model_path = 'model_data'
config_path = 'cfg'
yolo_url = 'https://pjreddie.com/media/files/'


def _main(args):
    weights = ''
    config = ''
    output = os.path.join(model_path, 'yolo.h5')
    download_path = os.path.join(model_path, 'download.sh')

    # YOLOv3 trained on COCO
    if args.yolo_version.upper() == 'YOLOV3':
        config = os.path.join(config_path, 'yolov3.cfg')
        weights = 'yolov3.weights'

    # YOLOv3 tinny trained on COCO
    elif args.yolo_version.upper() == 'YOLOT3':
        config = os.path.join(config_path, 'yolov3-tiny.cfg')
        weights = 'yolov3-tiny.weights'

    # YOLOv2 trained on COCO
    elif args.yolo_version.upper() == 'YOLOV2':
        config = os.path.join(config_path, 'yolov2.cfg')
        weights = 'yolov2.weights'

    # YOLOv2 trained on VOC
    elif args.yolo_version.upper() == 'YOLOV2-VOC':
        config = os.path.join(config_path, 'yolov2-voc.cfg')
        weights = 'yolov2-voc.weights'

    # YOLOv2 tinny trained on COCO
    elif args.yolo_version.upper() == 'YOLOT2':
        config = os.path.join(config_path, 'yolov2-tiny.cfg')
        weights = 'yolov2-tiny.weights'

    # YOLOv2 tinny trained on VOC
    elif args.yolo_version.upper() == 'YOLOT2-VOC':
        config = os.path.join(config_path, 'yolov2-tiny-voc.cfg')
        weights = 'yolov2-tiny-voc.weights'

    # YOLOv1 trained on VOC
    elif args.yolo_version.upper() == 'YOLOV1':
        config = os.path.join(config_path, 'yolov1 - tiny.cfg')
        weights = 'yolov1/tiny-yolov1.weights'

    # YOLOv1 tinny trained on VOC
    elif args.yolo_version.upper() == 'YOLOT1':
        config = os.path.join(config_path, 'yolov1.cfg')
        weights = 'yolov1/yolov1.weights'

    else:
        assert "Model doesn't exist!"

    model = os.path.join(model_path, weights)

    if not os.path.isfile(os.path.join(os.getcwd(), model)):
        print("Downloading weights %s" % weights)
        os.system("%s %s %s" % (download_path, yolo_url + weights, model_path))

    if not os.path.isfile(output):
        print("Creating model for: \n - configuration: %s\n - model: %s" % (config, model))
        os.system("python convert.py %s %s %s" % (config, model, output))
    else:
        overwrite = raw_input("File already exist, if you want overwrite write Y:")
        if overwrite.upper() == 'Y':
            print("Creating model for: \n - configuration: %s\n - model: %s" % (config, model))
            os.system("python convert.py %s %s %s" % (config, model, output))


if __name__ == '__main__':
    _main(parser.parse_args())
