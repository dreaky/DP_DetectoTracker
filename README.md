
DP_DetectoTracker
=========================

It is diploma project using all YOLO version as detector and DeepSort for tracking objects


Simple to run: 
------------------------- 
on Linux:

- Run initialization script with defined version of YOLO you want: 

      python initialize.py YOLOv3     
- Run detector:

      python run.py

on Windows:
- Download YOLO weights from YOLO website into folder model_data.
      
      https://pjreddie.com/darknet/yolo/
- Run converter to load Darknet model configuration and transfer it into Keras model

      python convert.py cfg/yolov3.cfg model_data/yolov3.weights model_data/yolo.h5    
- Run detector:

      python run.py
 
Dependencies: 
------------------------- 
    PIL
    keras
    cv2
    tensorflow
    scikit-learn
    scipy
    numpy



